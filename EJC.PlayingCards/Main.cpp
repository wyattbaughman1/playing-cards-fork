
// Playing Cards
// Ethan Corrigan

#include <iostream>
#include <conio.h>

using namespace std;

// Wyatt Playing Cards Fork from Ethan 

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};


enum Suit
{
	Hearts,
	Spades,
	Diamonds,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintRank(Card card)
{
	// Switch for Rank

	Card card;
	card.rank = Rank::Ace;

	switch (card.rank)
	{
	case Rank::Two:
		cout << "Two of ";
		break;
	case Rank::Three:
		cout << "Three of ";
		break;
	case Rank::Four:
		cout << "Four of ";
		break;
	case Rank::Five:
		cout << "Five of ";
		break;
	case Rank::Six:
		cout << "Six of ";
		break;
	case Rank::Seven:
		cout << "Seven of ";
		break;
	case Rank::Eight:
		cout << "Eight of ";
		break;
	case Rank::Nine:
		cout << "Nine of ";
		break;
	case Rank::Ten:
		cout << "Ten of ";
		break;
	case Rank::Jack:
		cout << "The Jack of ";
		break;
	case Rank::Queen:
		cout << "The Queen of ";
		break;
	case Rank::King:
		cout << "The King of ";
		break;
	case Rank::Ace:
		cout << "The Ace of ";
		break;
	default:
		break;
	}
}

void PrintSuit(Card card)
{
	// Switch for Suit

	Card card;
	card.suit = Suit::Hearts;

	switch (card.suit)
	{
	case Suit::Hearts:
		cout << "Hearts" << "\n";
		break;
	case Suit::Spades:
		cout << "Spades" << "\n";
		break;
	case Suit::Diamonds:
		cout << "Diamond" << "\n";
		break;
	case Suit::Clubs:
		cout << "Clubs" << "\n";
		break;
	default:
		break;
	}
}

void PrintCard(Card card)
{
	// Print Rank and suit

	PrintRank(card);
	PrintSuit(card);
}

Card HighCard(Card card1, Card card2)
{
	// Determines the higher card

	if (card1.rank > card2.rank)
	{
		return card1;
		PrintCard(card1);
	}
	else
	{
		return card2;
		PrintCard(card2);
	}
}

int main()
{
	Card card1;
	card1.rank = Rank::Ace;
	card1.suit = Suit::Hearts;

	Card card2;
	card2.rank = Rank::Ace;
	card2.suit = Suit::Hearts;
	
	PrintCard(card1);

	HighCard(card1, card2);

	(void)_getch();
	return 0;
}

